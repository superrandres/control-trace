import Vue from 'vue'
import VueGeolocation from 'vue-browser-geolocation'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'

Vue.config.productionTip = false
Vue.use(VueGeolocation)

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
