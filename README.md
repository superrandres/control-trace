## Los equipos o componentes de hardware que intervienen son:
1. Un teléfono móvil por agente vendedor con acceso a internet vía red móvil y con dispositivo GPS integrado.
2. Un servidor o varios si se requiere dividir la carga de trabajo con conexión en la misma red (LAN o WAN) de los teléfonos móviles.

## Los equipos o componentes de software que intervienen son:
1. Navegador Firefox o Chrome del teléfono móvil.
2. Mosquitto.
3. Node Red.
4. Influx DB.
5. Grafana.
6. Git.
7. Docker y Docker Compose

## Proceso de Instalación:
El proyecto se encuentra almacenado públicamente en BitBucket, así que empezaremos describiendo el proceso de clonación del repositorio para luego empezar con la instalación y configuración.

Desde un terminal ejecutamos:

1. Descargar el código fuentes:
git clone https://superrandres@bitbucket.org/superrandres/control-trace.git

2. Ingresar a la carpeta del código fuente descargado:
cd control-trace

3. Crear archivo de configuración:
nano .env
Pegar el siguiente contenido:
NODE_RED_CREDENTIAL_SECRET=strong-pass-secret
INFLUXDB_DB=control_trace
INFLUXDB_HTTP_AUTH_ENABLED=true
INFLUXDB_ADMIN_USER=root
INFLUXDB_ADMIN_PASSWORD=root123
INFLUXDB_USER=control_trace
INFLUXDB_USER_PASSWORD=control_trace123
INFLUXDB_READ_USER=control_trace_read
INFLUXDB_READ_USER_PASSWORD=control_trace_read123
INFLUXDB_WRITE_USER=control_trace_write
INFLUXDB_WRITE_USER_PASSWORD=control_trace_write123
GF_INSTALL_PLUGINS=grafana-worldmap-panel

4. Instalar las dependencias de la aplicación web:
docker-compose run --rm vuetify sh -c 'NODE_ENV=development npm i'

5. Levantar todos los servicios declarados en docker-compose.yml:
docker-compose up

Es proceso expondrá en su navegador los siguientes servicios:
1. Node Red: http://localhost:1880
2. Aplicación Web: http://localhost:8080/
3. Grafana: http://localhost:3000

Estos otro servicios se usaran desde Node Red:
1. Influx DB, puerto 8086.
2. Mosquitto, puerto 1883.

## Ingresar a Influx DB shell:
docker-compose exec influx-db influx -username=root --password ''